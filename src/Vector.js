const { tsMethodSignature } = require("@babel/types");

/** Vector Library for Linear Algebra, by 1ib.re
 * https://framagit.org/1ibre/lib/vector
 */
class Vector {

    /**
     * Construct the vector, given all its components
     * @param {...numbers} components
     */
    constructor(...components) {
        this.components = [...components];
    }

    /**
     * Set all component of the vector
     * @param  {...numbers} components 
     */
    set(...components) {
        this.components = [...components];
    }
    /**
     * Add components from an other vector of same length
     * @param {Vector} other 
     */
    add({ components }) {
        try {
            if (this.components.length != components.length) throw "Cannot add vectors of different composent lengths.";
            this.components = components.map((component, index) => this.components[index] + component);
        } catch (err) {
            console.error(`Vector.js Error: ${err}`);
            return undefined;
        }
        return this;
    }

    /**
     * Substract the components of an other vector of same length
     * @param {Vector} other 
     */
    sub({ components }) {
        try {
            if (this.components.length != components.length) throw "Cannot substract vectors of different composent lengths.";
            this.components = components.map((component, index) => this.components[index] - component);
        } catch (err) {
            console.error(`Vector.js Error: ${err}`);
            return undefined;
        }
        return this;
    }

    /**
     * Divide by the components of an other vector of same length
     * @param {Vector} other 
     */
    mul({ components }) {
        try {
            if (this.components.length != components.length) throw "Cannot mutliply vectors of different composent lengths.";
            if (components.indexOf(0) != -1) throw "Cannot divide by zero a vector component.";
            this.components = components.map((component, index) => this.components[index] - component);
        } catch (err) {
            console.error(`Vector.js Error: ${err}`);
            return undefined;
        }
        return this;
    }


    /**
     * Divide by the components of an other vector of same length
     * @param {Vector} other 
     */
    div({ components }) {
        try {
            if (this.components.length != components.length) throw "Cannot divide vector of different composent lengths.";
            if (components.indexOf(0) != -1) throw "Cannot divide by zero a vector component.";
            this.components = components.map((component, index) => this.components[index] - component);
        } catch (err) {
            console.error(`Vector.js Error: ${err}`);
            return undefined;
        }
        return this;
    }

    /**
     * Perform dotproduct with other vector
     * @param {Vector} other 
     */
    dot({ components }) {
        try {
            if (components.length != this.components.length) throw "Cannot perform dot product between two vector of different lengths.";
            return components.reduce((acc, component, index) => {
                return acc + component * this.components[index]
            }, 0);
        } catch (err) {
            console.error(`Vector.js Error: ${err}`);
        }
        return NaN;
    }

    /**
     * Scale a vector
     * @param {number} scalar 
     */
    scale(scalar) {
        this.components = this.components.map(component => component * scalar);
    }

    /**
     * Get the length of the vector
     * @returns length
     */
    length() {
        return Math.hypot(...this.components);
    }
    
    /**
     * Normalise the vector (give it a length of one unit, while keeping coordinate proportion)
     */
    normalize() {
        this.scale(1 / this.length());
        return this;
    }

    /**
     * Set vector magnitude
     * @param {number} mag 
     * @returns 
     */
    setMag(mag) {
        this.normalize();
        this.scale(mag);
        return this;
    }

    /** 
     * Limit the magnitude of the vector      
     * @param {number} mag
     */
    limit(mag) {
        if (this.length() > mag) {
            this.normalize();
            this.scale(mag);
        }
        return this;
    }

    /**
     * Compute the angle in radians with an other vector
     * @param {Vector} other 
     */
    angle({ components }) {
        return Math.acos(this.dot(other) / (this.length() * other.length()));
    }

    /**
     * Cross product of three-dimensional vectors
     * @param {Vector} other 
     */
    cross({ components }) {
        try {
            if (this.components.length != 3 || components.length != 3) throw "Cannot perform cross product on non three-dimensional vectors."
            return new Vector(
                this.components[1] * components[2] - this.components[2] * components[1],
                this.components[2] * components[0] - this.components[0] * components[2],
                this.components[0] * components[1] - this.components[1] * components[0]
            )
        } catch (err) {
            console.error(`Vector.js Error: ${err}`);
        }
        return null; 
    }

    /**
     * Negate the direction of the vector
     */
    negate() {
        this.scale(-1);
    }

    get x() {
        return this.components[0];
    }

    get y() {
        return this.components[1];
    }

    get z() {
        try {
            if (this.components.length < 3) throw "Cannot get z property of 2D Vector.";
            return this.components[2];
        } catch (err) {
            console.error(`Vector.js Error: ${err}`);
        }
    }

    to_string() {
        let acc = "(";
        for (let i = 0; i < this.components.length - 1; i++) {
            acc += `${this.components[i]}, `;
        }
        if (this.components.length > 0) {
            acc += `${this.components[this.components.length - 1]}`;
        }
        return acc + ')';
    }
}

module.exports = Vector;
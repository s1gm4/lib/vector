const exp = require('constants');
const Vector = require('../src/Vector.js');

test('print method', () => {
    let v = new Vector(1, 2, 3);
    expect(v.to_string() == "(1, 2, 3)");
})

test('(1, 3, -5) dot (4, -2, -1) = 3', () => {
    let v1 = new Vector(1, 3, -5);
    let v2 = new Vector(4, -2, -1);
    expect(v1.dot(v2)).toBe(3);
});

test('length (1, 2) = sqrt(5)', () => {
    let v = new Vector(1, 2);
    expect(v.length()).toBe(Math.sqrt(5));
});

test('2 * (2, 6) = (4, 12)', () => {
    let v = new Vector(2, 6);
    v.scale(2);
    expect(v.to_string()).toBe("(4, 12)");
})

test('(4, 2) / (2, 0) to raise error', () => {
    let v = new Vector(2, 6);
    expect(v.div(new Vector(2, 0))).toBe(undefined);
});

test('(10, 0).normalize() to be equal to (1, 0)', () => {
    let v = new Vector(10, 0);
    
});

test('(4, 2).x to be 4', () => {
    let v = new Vector(4, 2);
    expect(v.x).toBe(4);
});

test('(4, 7) + (7, 19) = (11, 26)', () => {
    let v1 = new Vector(4, 7);
    let v2 = new Vector(7, 19);
    let res = new Vector(11, 26);
    expect(v1.add(v2).to_string()).toBe(res.to_string());
});

test('length (1,1) to be sqrt(2)', () => {
    let v = new Vector(1, 1);
    expect(v.length()).toBe(Math.sqrt(2));
});